import requests
import pandas as pd
from datetime import datetime, timedelta
from concurrent.futures import ThreadPoolExecutor, as_completed

def fetch_datagrepper_page(base_url, params):
    for attempt in range(5):
        try:
            response = requests.get(base_url, params=params)
            response.raise_for_status()
            data = response.json()
            return data['raw_messages'], params['page']
        except (requests.exceptions.RequestException, requests.exceptions.ProxyError) as e:
            print(f"Attempt {attempt + 1} failed for page {params['page']}: {e}")
            if attempt == 4:
                raise
            else:
                continue

def fetch_datagrepper_data_for_day(date):
    base_url = "https://apps.fedoraproject.org/datagrepper/v2/search"
    end_date = (datetime.strptime(date, "%Y-%m-%d") + timedelta(days=1)).strftime("%Y-%m-%d")
    params = {
        'start': date,
        'end': end_date,
        'page': 1,
        'rows_per_page': 100
    }
    
    for attempt in range(5):
        try:
            response = requests.get(base_url, params=params)
            response.raise_for_status()
            data = response.json()
            break
        except (requests.exceptions.RequestException, requests.exceptions.ProxyError) as e:
            print(f"Attempt {attempt + 1} failed for initial request: {e}")
            if attempt == 4:
                raise
            else:
                continue

    total_pages = data['pages']
    all_results = data['raw_messages']

    if total_pages > 1:
        with ThreadPoolExecutor(max_workers=50) as executor:
            page_futures = []
            
            # Submit remaining page requests
            for page in range(2, total_pages + 1):
                params_copy = params.copy()
                params_copy['page'] = page
                future = executor.submit(fetch_datagrepper_page, base_url, params_copy)
                page_futures.append(future)
            
            for future in as_completed(page_futures):
                try:
                    page_results, page_number = future.result()
                    print(f"Fetched data for {date}, page {page_number}")
                    all_results.extend(page_results)
                except Exception as e:
                    print(f"An error occurred: {e}")

    return all_results

def normalize_data(data):
    for record in data:
        for key, value in record.items():
            if isinstance(value, dict):
                record[key] = str(value)
    return data

def save_data_to_parquet(data, date):
    normalized_data = normalize_data(data)
    df = pd.DataFrame(normalized_data)
    file_name = f"fedora-{date.replace('-', '')}.parquet"
    df.to_parquet(file_name, index=False, compression='zstd')
    print(f"Data saved to {file_name}")

def fetch_and_save_data(start_date, end_date):
    current_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    
    while current_date <= end_date:
        date_str = current_date.strftime("%Y-%m-%d")
        data = fetch_datagrepper_data_for_day(date_str)
        save_data_to_parquet(data, date_str)
        current_date += timedelta(days=1)


#### Example usage
start_date = "2024-01-01"
end_date = "2024-06-01"

fetch_and_save_data(start_date, end_date)
####